package com.hello.sarang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SarangApplication {

    public static void main(String[] args) {
        SpringApplication.run(SarangApplication.class, args);
    }

}
